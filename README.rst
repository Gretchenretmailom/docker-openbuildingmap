==============================
Docker Compose OpenBuildingMap
==============================

This is a `Docker Compose <https://docs.docker.com/compose/>`__ setup for
the `OpenBuildingMap <https://openbuildingmap.org>`__ stack.

It contains and assembles:

* a database (`Docker OBM Database <https://gitext.gfz-potsdam.de/dynamicexposure/server-components/containers/docker-obm-database>`__)
* an importer and updater service (`Docker OBM Importer <https://gitext.gfz-potsdam.de/dynamicexposure/server-components/containers/docker-obm-importer>`__)
* a calculation engine (in progress)
* a raster tile server  (`Docker OBM Tiles Raster <https://gitext.gfz-potsdam.de/dynamicexposure/server-components/containers/docker-obm-tiles-raster>`__)
* a vector tile server (in progress)

Requirements
------------

The following dependencies must be installed on the system:

* `Docker <https://www.docker.com/>`__
* `Docker Compose <https://docs.docker.com/compose/>`__


Run the setup
-------------

One command only to get all up and running:

* :code:`$ docker-compose up -d`

Inspect the logs

* :code:`$ docker-compose logs -f`


Copyright and copyleft
----------------------

Copyright (C) 2020

* Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum GFZ

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.

Also add information on how to contact you by electronic and paper mail.

If your software can interact with users remotely through a computer
network, you should also make sure that it provides a way for users to
get its source. For example, if your program is a web application, its
interface could display a "Source" link that leads users to an archive
of the code. There are many ways you could offer source, and different
solutions will be better for different programs; see section 13 for the
specific requirements.

See the `LICENSE <./LICENSE>`__ for the full license text.
